package org.igov.staff.dao;

import org.igov.staff.model.StatusBP;
import org.wf.dp.dniprorada.base.dao.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface StatusBpDao extends EntityDao<StatusBP> {
}
