package org.igov.staff.model;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.wf.dp.dniprorada.base.model.Entity;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class SubjectReport_Structure extends Entity {

    @JsonProperty(value = "oStructure")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Structure", nullable = false)
    private Structure oStructure;

    @JsonProperty(value = "nID_Organ")
    private Long nID_Organ;

    @JsonProperty(value = "oPeriod")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Period", nullable = false)
    private Period oPeriod;

    @JsonProperty(value = "aSubjectReport_Position")
    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OrderBy("order asc")
    private List<SubjectReport_Position> aSubjectReport_Position = new ArrayList<SubjectReport_Position>();

    @JsonProperty(value = "nCountOrgan")
    @Column
    private Integer nCountOrgan;

    public Long getnID_Organ() {
        return nID_Organ;
    }

    public void setnID_Organ(Long nID_Organ) {
        this.nID_Organ = nID_Organ;
    }

    public Structure getoStructure() {
        return oStructure;
    }

    public void setoStructure(Structure oStructure) {
        this.oStructure = oStructure;
    }

    public Period getoPeriod() {
        return oPeriod;
    }

    public void setoPeriod(Period oPeriod) {
        this.oPeriod = oPeriod;
    }

    public List<SubjectReport_Position> getaSubjectReport_Position() {
        return aSubjectReport_Position;
    }

    public void setaSubjectReport_Position(
            List<SubjectReport_Position> aSubjectReport_Position) {
        this.aSubjectReport_Position = aSubjectReport_Position;
    }

    public Integer getnCountOrgan() {
        return nCountOrgan;
    }

    public void setnCountOrgan(Integer nCountOrgan) {
        this.nCountOrgan = nCountOrgan;
    }
}
