package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.LanchBP;
import org.springframework.stereotype.Repository;
import org.wf.dp.dniprorada.base.dao.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class LanchBpDaoImpl extends GenericEntityDao<LanchBP> implements LanchBpDao {

    private static final Logger log = Logger.getLogger(LanchBpDaoImpl.class);

    public LanchBpDaoImpl() {
        super(LanchBP.class);
    }
}
