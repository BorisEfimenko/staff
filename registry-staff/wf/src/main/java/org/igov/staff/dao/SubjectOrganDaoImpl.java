package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.SubjectOrgan;
import org.springframework.stereotype.Repository;
import org.wf.dp.dniprorada.base.dao.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class SubjectOrganDaoImpl extends GenericEntityDao<SubjectOrgan> implements SubjectOrganDao  {

    private static final Logger log = Logger.getLogger(SubjectOrganDaoImpl.class);

    protected SubjectOrganDaoImpl() {
        super(SubjectOrgan.class);
    }
}
