package org.igov.staff.dao;

import org.igov.staff.model.SubjectOrgan;
import org.wf.dp.dniprorada.base.dao.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface SubjectOrganDao extends EntityDao<SubjectOrgan> {
}
