package org.igov.staff.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.wf.dp.dniprorada.base.model.Entity;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class SubjectReport_Position extends Entity {

    @JsonProperty(value = "oPosition")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Position", nullable = false)
    private Position oPosition;

    @JsonProperty(value = "aSubjectReport_Category")
    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OrderBy("order asc")
    private List<SubjectReport_Category> aSubjectReport_Category = new ArrayList<SubjectReport_Category>();

    @JsonProperty(value = "nCountHired_Re")
    @Column
    private Integer nCountHired_Re;

    @JsonProperty(value = "nCountHiredCompet_Re")
    @Column
    private Integer nCountHiredCompet_Re;

    public Position getoPosition() {
        return oPosition;
    }

    public void setoPosition(Position oPosition) {
        this.oPosition = oPosition;
    }

    public Integer getnCountHired_Re() {
        return nCountHired_Re;
    }

    public void setnCountHired_Re(Integer nCountHired_Re) {
        this.nCountHired_Re = nCountHired_Re;
    }

    public Integer getnCountHiredCompet_Re() {
        return nCountHiredCompet_Re;
    }

    public void setnCountHiredCompet_Re(Integer nCountHiredCompet_Re) {
        this.nCountHiredCompet_Re = nCountHiredCompet_Re;
    }

    public List<SubjectReport_Category> getaSubjectReport_Category() {
        return aSubjectReport_Category;
    }

    public void setaSubjectReport_Category(
            List<SubjectReport_Category> aSubjectReport_Category) {
        this.aSubjectReport_Category = aSubjectReport_Category;
    }
    
}
