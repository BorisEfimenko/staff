package org.igov.staff.dao;

import org.igov.staff.model.SubjectReport_Structure;
import org.wf.dp.dniprorada.base.dao.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface SubjectReport_StructureDao extends EntityDao<SubjectReport_Structure> {
}
