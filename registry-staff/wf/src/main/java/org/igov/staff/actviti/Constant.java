package org.igov.staff.actviti;

public class Constant {

    public static final String organ_ID = "act_id_group.id_";
    public static final String organName = "act_id_group.name_";
    public static final String sIndex = "subjectOrgan.index";
    public static final String sRegion = "subjectOrgan.region";
    public static final String sCity = "subjectOrgan.city";
    public static final String sDistrict = "subjectOrgan.district";
    public static final String sStreet = "subjectOrgan.street";
    public static final String sBuild = "subjectOrgan.build";
    public static final String sPhone = "subjectOrgan.phone";
    public static final String sFax = "subjectOrgan.fax";
    public static final String sEmail = "subjectOrgan.email";
    public static final String sLast = "act_id_user.last";
    public static final String sFirst = "act_id_user.first";
    public static final String sMiddle = "act_id_user.middle";
    public static final String sLogin = "act_id_user.login";
    public static final String sPassword = "act_id_user.password";
    public static final String sPassword2 = "act_id_user.password2";
    
}
