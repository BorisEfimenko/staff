package org.igov.staff.dao;

import org.igov.staff.model.Structure;
import org.igov.staff.model.StructureOrgan;
import org.wf.dp.dniprorada.base.dao.EntityDao;

import java.util.List;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
public interface StructureOrganDao extends EntityDao<StructureOrgan> {

    public List<Structure> getStructureByID_Organ(Long id);
}
