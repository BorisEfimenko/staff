package org.igov.staff.rest.controller;

import org.activiti.rest.service.api.runtime.process.ExecutionBaseResource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ...wf/service/... Example:
 * .../wf/service/rest/startProcessByKey/citizensRequest
 */
@Controller
@RequestMapping(value = "/rest")
public class ActivitiRestController extends ExecutionBaseResource {
     
    @RequestMapping(value = "/getOrganName", method = RequestMethod.GET)
    @Transactional
    public
    @ResponseBody
    String getOrganName() {
       return "test"; 
    }
}
