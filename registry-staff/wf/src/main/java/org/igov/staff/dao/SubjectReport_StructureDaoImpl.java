package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.SubjectReport_Structure;
import org.springframework.stereotype.Repository;
import org.wf.dp.dniprorada.base.dao.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class SubjectReport_StructureDaoImpl extends GenericEntityDao<SubjectReport_Structure> implements SubjectReport_StructureDao{

    private static final Logger log = Logger.getLogger(PositionDaoImpl.class);

    protected SubjectReport_StructureDaoImpl() {
        super(SubjectReport_Structure.class);
    }
}
