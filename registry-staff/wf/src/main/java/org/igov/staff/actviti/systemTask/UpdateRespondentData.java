package org.igov.staff.actviti.systemTask;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.igov.staff.dao.SubjectOrganDao;
import org.igov.staff.model.SubjectOrgan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component("updateRespondentData")
public class UpdateRespondentData implements JavaDelegate {
    
    private final static Logger LOG = LoggerFactory.getLogger(UpdateRespondentData.class);
    
    @Autowired
    private SubjectOrganDao subjectOrganDao;
    

    private SubjectOrgan subjectOrganExt;
    
    @Override
    public void execute(DelegateExecution oExecution) throws Exception {
        
    }
}
