package org.igov.staff.model;

import javax.persistence.Column;
import org.wf.dp.dniprorada.base.model.NamedEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

@javax.persistence.Entity
public class SubjectOrgan extends NamedEntity {

    @JsonProperty(value = "sAddress")
    @Column
    private String sAdress;

    @JsonProperty(value = "sIndex")
    @Column
    private String sIndex;

    @JsonProperty(value = "sPhone")
    @Column
    private String sPhone;

    @JsonProperty(value = "sFax")
    @Column
    private String sFax;

    @JsonProperty(value = "sEmail")
    @Column
    private String sEmail;

    public String getsAdress() {
        return sAdress;
    }

    public void setsAdress(String sAdress) {
        this.sAdress = sAdress;
    }

    public String getsPhone() {
        return sPhone;
    }

    public void setsPhone(String sPhone) {
        this.sPhone = sPhone;
    }

    public String getsFax() {
        return sFax;
    }

    public void setsFax(String sFax) {
        this.sFax = sFax;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getsIndex() {
        return sIndex;
    }

    public void setsIndex(String sIndex) {
        this.sIndex = sIndex;
    }

}
