package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.Period;
import org.springframework.stereotype.Repository;
import org.wf.dp.dniprorada.base.dao.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class PeriodDaoImpl extends GenericEntityDao<Period> implements PeriodDao {

    private static final Logger log = Logger.getLogger(PeriodDaoImpl.class);

    protected PeriodDaoImpl() {
        super(Period.class);
    }
}
