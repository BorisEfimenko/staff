package org.igov.staff.model;

import org.hibernate.annotations.Type;
import org.wf.dp.dniprorada.base.model.Entity;
import org.wf.dp.dniprorada.base.util.JsonDateDeserializer;
import org.wf.dp.dniprorada.base.util.JsonDateSerializer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

import javax.persistence.Column;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class Period extends Entity {
    
    @JsonProperty(value = "nYear")
    @Column
    private Integer nYear      ;
    
    @JsonProperty(value = "nQuater")
    @Column
    private Integer nQuater    ;
    
    @JsonProperty(value = "sDateStart")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private Date    oDateStart ;
    
    @JsonProperty(value = "sDateFinish")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private Date    oDateFinish;

    public Integer getnYear() {
        return nYear;
    }

    public void setnYear(Integer nYear) {
        this.nYear = nYear;
    }

    public Integer getnQuater() {
        return nQuater;
    }

    public void setnQuater(Integer nQuater) {
        this.nQuater = nQuater;
    }

    public Date getoDateStart() {
        return oDateStart;
    }

    public void setoDateStart(Date oDateStart) {
        this.oDateStart = oDateStart;
    }

    public Date getoDateFinish() {
        return oDateFinish;
    }

    public void setoDateFinish(Date oDateFinish) {
        this.oDateFinish = oDateFinish;
    }
}
