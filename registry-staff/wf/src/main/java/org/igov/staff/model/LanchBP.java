package org.igov.staff.model;

/**
 * Created by Sergey_PC on 24.10.2015.
 */

import org.hibernate.annotations.Type;
import org.wf.dp.dniprorada.base.model.Entity;
import org.wf.dp.dniprorada.base.util.JsonDateDeserializer;
import org.wf.dp.dniprorada.base.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.*;

@javax.persistence.Entity
public class LanchBP extends Entity {

    @JsonProperty(value = "sID_BP")
    @Column
    private String sID_BP;

    @JsonProperty(value = "oStatusBP")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_StatusBP", nullable = false)
    private StatusBP oStatusBP;

    @JsonProperty(value = "sDateStart")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private Date oDateStart;

    @JsonProperty(value = "sDateFinish")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private Date oDateFinish;

    @JsonProperty(value = "oSubjectReport_Structure")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_SubjectReport_Structure", nullable = false)
    private SubjectReport_Structure oSubjectReport_Structure;

    public String getsID_BP() {
        return sID_BP;
    }

    public void setsID_BP(String sID_BP) {
        this.sID_BP = sID_BP;
    }

    public Date getoDateStart() {
        return oDateStart;
    }

    public void setoDateStart(Date oDateStart) {
        this.oDateStart = oDateStart;
    }

    public Date getoDateFinish() {
        return oDateFinish;
    }

    public void setoDateFinish(Date oDateFinish) {
        this.oDateFinish = oDateFinish;
    }

}
