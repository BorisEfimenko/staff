package org.igov.staff.dao;

import org.igov.staff.model.SubjectReport_Category;
import org.wf.dp.dniprorada.base.dao.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface SubjectReport_CategoryDao extends EntityDao<SubjectReport_Category> {
}
