package org.igov.staff.actviti.systemTask;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;
import org.igov.staff.actviti.Constant;
import org.igov.staff.constant.ConstantUtil;
import org.igov.staff.dao.SubjectOrganDao;
import org.igov.staff.model.SubjectOrgan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component("saveRespondentData")
public class SaveRespondentData implements JavaDelegate {
    
    private final static Logger LOG = LoggerFactory.getLogger(SaveRespondentData.class);
    
    @Autowired
    private SubjectOrganDao subjectOrganDao;

    private SubjectOrgan subjectOrganExt;
    
    @Override
    public void execute(final DelegateExecution execution) throws Exception {  
        
        //сохранение данных респондента
        subjectOrganExt = new SubjectOrgan();
        subjectOrganExt.setName((String)execution.getVariable(Constant.organ_ID));
        subjectOrganExt.setsPhone((String)execution.getVariable(Constant.sPhone));
        subjectOrganExt.setsEmail((String)execution.getVariable(Constant.sEmail));
        subjectOrganExt.setsFax((String)execution.getVariable(Constant.sFax));
        subjectOrganExt.setsIndex((String)execution.getVariable(Constant.sIndex));
        String adress = new StringBuilder()
            .append(Constant.sRegion).append(ConstantUtil.separator)
            .append(Constant.sCity).append(ConstantUtil.separator)
            .append(Constant.sDistrict).append(ConstantUtil.separator)
            .append(Constant.sStreet).append(ConstantUtil.separator)
            .append(Constant.sBuild).append(ConstantUtil.separator).toString();
        subjectOrganExt.setsAdress(adress);
        subjectOrganDao.saveOrUpdate(subjectOrganExt);
        
        //создание нового пользователя
        User user = new User() {
            
            @Override
            public void setPassword(String string) {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void setLastName(String lastName) {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void setId(String id) {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void setFirstName(String firstName) {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void setEmail(String email) {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public boolean isPictureSet() {
                return false;
            }
            
            @Override
            public String getPassword() {
                return (String)execution.getVariable(Constant.sPassword);
            }
            
            @Override
            public String getId() {
                return (String)execution.getVariable(Constant.sLogin);
            }
            
            @Override
            public String getLastName() {
                return (String)execution.getVariable(Constant.sLast);
            }
            
            @Override
            public String getFirstName() {
                return (String)execution.getVariable(Constant.sFirst) 
                        + " " + (String)execution.getVariable(Constant.sMiddle);
            }
            
            @Override
            public String getEmail() {
                return (String)execution.getVariable(Constant.sEmail);
            }
        };
        execution.getEngineServices().getIdentityService().saveUser(user);
        
        //добавление пользователя в группу
        execution.getEngineServices().getIdentityService().createMembership(user.getId(), 
                (String)execution.getVariable(Constant.organ_ID));
    }
}
