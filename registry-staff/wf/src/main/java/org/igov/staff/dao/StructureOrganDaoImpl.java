package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.igov.staff.model.Structure;
import org.igov.staff.model.StructureOrgan;
import org.wf.dp.dniprorada.base.dao.GenericEntityDao;

import java.util.List;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
public class StructureOrganDaoImpl  extends GenericEntityDao<StructureOrgan>  implements StructureOrganDao{

    private static final Logger log = Logger.getLogger(StructureOrgan.class);

    protected StructureOrganDaoImpl() {
        super(StructureOrgan.class);
    }

    @Override
    public List<Structure> getStructureByID_Organ(Long id) {
        StructureOrgan structureOrgan = null;
        Criteria criteria = getSession().createCriteria(StructureOrgan.class);
        criteria.add(Restrictions.eq("nID_Organ", id));
        structureOrgan = (StructureOrgan) criteria.uniqueResult();
        return structureOrgan.getaStructure();
    }
}
