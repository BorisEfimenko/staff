package org.igov.staff.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.wf.dp.dniprorada.base.model.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@javax.persistence.Entity
@Table(name = "StructureOrgan")
public class StructureOrgan extends Entity {
    
    @JsonProperty(value = "nId_Organ")
    @Column
    private Long nId_Organ;

    @JsonProperty(value = "aStructure")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Structure> aStructure = new ArrayList<Structure>();

    public Long getnId_Organ() {
        return nId_Organ;
    }

    public void setnId_Organ(Long nId_Organ) {
        this.nId_Organ = nId_Organ;
    }

    public List<Structure> getaStructure() {
        return aStructure;
    }

    public void setaStructure(List<Structure> aStructure) {
        this.aStructure = aStructure;
    }

}
