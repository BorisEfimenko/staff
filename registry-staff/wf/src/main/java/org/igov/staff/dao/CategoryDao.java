package org.igov.staff.dao;

import org.igov.staff.model.Category;
import org.wf.dp.dniprorada.base.dao.EntityDao;

public interface CategoryDao extends EntityDao<Category> {

}

